package PlanetHub;

import java.io.*;
import java.util.*;


public class RunHub {

    public static LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> getPlanetData() throws Exception {
        return PlanetData.planetData();
    }

    public static void main(String[] args) throws Exception {
        System.out.println(getPlanetData().get("EARTH") + "\n");
        System.out.println(getPlanetData().get("MARS"));
    }
}

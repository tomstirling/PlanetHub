package PlanetHub;

import java.io.*;
import java.util.*;

public class PlanetData {

    private static final String DATA_FILE = "data/planet-data.txt";

    // Tidy up and extract some to new class - especially the ArrayList creation.
    public static LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> planetData() {
        LinkedHashMap<String, ArrayList<LinkedHashMap<String,String>>> data = new LinkedHashMap<>();

        try {
            BufferedReader input = new BufferedReader(new FileReader(DATA_FILE));
            String currentLine; String firstLine = input.readLine(); String[] lineData = new String[8];
            ArrayList<LinkedHashMap<String,String>> subData = new ArrayList<>(); int planet = 0; 

            for(int i=0; i<getPlanetNames(firstLine).length; i++) { data.put(getPlanetNames(firstLine)[i], null); }

            while((currentLine = input.readLine()) != null) {      
                for(int i=0; i<10; i++) {
                    LinkedHashMap<String,String> l1 = new LinkedHashMap<>();
                    l1.put(getStatName(currentLine), getPlanetData(currentLine)[i]);
                    subData.add(l1);
                }
            }

            for (Map.Entry<String, ArrayList<LinkedHashMap<String,String>>> entry : data.entrySet()) {
                String key = entry.getKey(); ArrayList<LinkedHashMap<String,String>> l2 = new ArrayList<>();
                for(int i=planet; i<subData.size(); i+=10) {
                    if(!key.contains(subData.get(i).toString())) {
                        l2.add(subData.get(i)); 
                        continue;  
                    }
                }
                data.put(key, l2);
                planet++;
            }
            
            input.close();
           
        } catch (IOException e) { e.printStackTrace(); } 

        return data;
    }

    public static String[] getPlanetNames(String line) {
        return line.split(":", 2)[1].split(", ");
    }

    public static String getStatName(String line) {
        return line.split(":", 2)[0].toString();
    }
    
    private static String[] getPlanetData(String line) {
        String[] full = line.split(":", 2);
        String[] parsed = full[1].split(", ");

        return parsed;
    }
}
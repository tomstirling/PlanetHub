## Planet-Hub (abbrv. PTH)

### A Java program that provides factual information on the planetary inhabitants of our solar system.

**Thomas Stirling - MEng CS student at the University of Leeds.**